<?php

namespace CodeDelivery\Http\Controllers\Api\Client;

use CodeDelivery\Repositories\ProductRepository;
use CodeDelivery\Http\Controllers\Controller;


class ClientProductController extends Controller
{

    private $repository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->repository = $productRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->repository->skipPresenter(false)->all();
        return $products;
    }
}
