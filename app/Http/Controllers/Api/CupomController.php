<?php

namespace CodeDelivery\Http\Controllers\Api;

use CodeDelivery\Repositories\CupomRepository;
use CodeDelivery\Repositories\ProductRepository;
use CodeDelivery\Http\Controllers\Controller;


class CupomController extends Controller
{

    private $repository;

    public function __construct(CupomRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($code)
    {
        return $this->repository->skipPresenter(false)->findByCode($code);
    }
}
