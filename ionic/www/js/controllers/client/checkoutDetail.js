/**
 * Created by rothink on 13/11/16.
 */
angular.module('starter.controllers')
    .controller('ClientCheckoutDetailCtrl', [
        '$scope', '$state', '$cart', '$stateParams', function($scope, $state, $cart, $stateParams) {

        $scope.updateQtd = function() {
            $cart.updateQtd($stateParams.index,$scope.product.qtd);
            $state.go('client.checkout');
        };

        $scope.product = $cart.getItem($stateParams.index);



    }]);