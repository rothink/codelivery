/**
 * Created by rothink on 13/11/16.
 */
angular.module('starter.controllers')
    .controller('ClientCheckoutCtrl', [
        '$scope', '$state', '$cart','Order', '$ionicLoading', '$ionicPopup', 'Cupom', '$cordovaBarcodeScanner', 'User' ,
        function($scope, $state, $cart, Order, $ionicLoading, $ionicPopup, Cupom, $cordovaBarcodeScanner, User) {

            User.authenticated({include: 'client'}, function(data) {
                console.info(data.data);
            }, function(responseError){

            });

            var cart = $cart.get();

            $scope.cupom = cart.cupom;
            $scope.items = cart.items;
            $scope.total = $cart.getTotalFinal();

            $scope.removeItem = function(i) {
                $cart.removeItem(i);
                $scope.items.splice(i,1);
                $scope.total = $cart.getTotalFinal();
            };

            $scope.openProductDetail = function(i) {
                $state.go('client.checkout_item_detail',{index : i});
            };

            $scope.openListProducts = function () {
                $state.go('client.view_products');
            };

            $scope.save = function() {
                var o = {
                    items: angular.copy($scope.items)
                };

                angular.forEach(o.items, function(item){
                    item.product_id = item.id;
                });

                $ionicLoading.show({
                    template : 'Salvando...'
                });

                if($scope.cupom.value) {
                    o.cupom_code = $scope.cupom.code;
                }

                Order.save({id:null}, o, function(data){
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        template: 'Salvo com sucesso'
                    });
                    $state.go('client.checkout_successful');
                }, function(error){
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: 'Advertência',
                        template: 'Pedido não realizado. Tente novamente',
                        buttons: [
                            {
                                text: '<b>Fechar</b>',
                                type: 'button-assertive'
                            }
                        ]
                    });
                });
            };

            $scope.readBarCode = function() {
                $cordovaBarcodeScanner
                    .scan()
                    .then(function(barcodeData) {
                        getValueCupom(barcodeData.text);
                        // Success! Barcode data is here
                    }, function(error) {
                        // An error occurred
                    });

            };

            $scope.removeCupom = function() {
                $cart.removeCupom();
                $scope.cupom = $cart.get().cupom;
                $scope.total = $cart.getTotalFinal();
            };

            function getValueCupom(code) {
                $ionicLoading.show({
                    template : 'Verificando...'
                });

                Cupom.get({
                    code:code
                }, function(data){
                    $cart.setCupom(data.data.code, data.data.value);
                    $scope.cupom = $cart.get().cupom;
                    $scope.total = $cart.getTotalFinal();
                    $ionicLoading.hide();
                }, function(error){
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: 'Advertência',
                        template: 'Cupom inválido. Tente novamente',
                        buttons: [
                            {
                                text: '<b>Fechar</b>',
                                type: 'button-assertive'
                            }
                        ]
                    });
                });
            };
    }]);