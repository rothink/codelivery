/**
 * Created by rothink on 13/11/16.
 */
angular.module('starter.controllers')
    .controller('ClientMenuCtrl', ['$scope', '$state', '$ionicLoading', 'User',
        function($scope, $state, $ionicLoading, User) {

            $scope.user = {
                name: ''
            };

            $ionicLoading.show({
                template: 'Carregando...'
            });

            User.authenticated({},function(user){
                $scope.user = user.data;
                $ionicLoading.hide();
            }, function (error) {
                $ionicLoading.hide();
            });
        }
    ]);